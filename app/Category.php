<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    protected $fillable =['name','img_url'];

    public function subcategories(){
        return $this->hasMany('App\Subcategory');
    }

    public function products(){
        return $this->hasMany('App\Product');
    }

    public function collection(){
        return $this->belongsTo('App\Collection');
    }
}
