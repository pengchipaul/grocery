<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function api_get_orders_by_status($status){
        $user = Auth::user();
        $orders = $user->orders->where('status',$status)->all();
        return response()->json($orders,200);
    }

    public function api_index(){
        $user = Auth::user();
        $orders = $user->orders;
        foreach($orders as $order){
            foreach($order->products as $product){
                $product['quantity'] = OrderProduct::where(['order_id' => $order->id, 'product_id' => $product->id])->first()->quantity;
            }
            $order['products'] = $order->products;
        }
        return response()->json($orders,200);
    }

    public function api_show($id){
        $user = Auth::user();
        $order = $user->orders->find($id);
        foreach($order->products as $product){
            $product['quantity'] = OrderProduct::where(['order_id' => $id, 'product_id' => $product->id])->first()->quantity;
        }
        $order['products'] = $order->products;
        $order['address'] = $order->address;
        return response()->json($order,200);
    }
}
