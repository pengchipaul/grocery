<?php

namespace App\Http\Controllers;

use App\Subcategory;
use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subcategories = Subcategory::all();
        return view('products.create',compact('subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();
        if($request->hasfile('img_url'))
        {
            if($request->file('img_url')->isValid()){
                $img_path = $request->file('img_url')->store('product_img');
            } else {
                return redirect('products/create')->with('fail','图片上传失败');
            }

        }
        $product->name = $request->get('name');
        if(isset($img_path)){
            $product->img_url = $img_path;
        }
        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->subcategory_id = $request->get('subcategory_id');
        $product->category_id = Subcategory::find($product->subcategory_id)->category->id;
        $product->save();

        return redirect('products')->with('success','已添加产品');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategories = Subcategory::all();
        $product = Product::find($id);
        return view('products.edit',compact('subcategories','product','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if($request->hasfile('img_url'))
        {
            if($request->file('img_url')->isValid()){
                $img_path = $request->file('img_url')->store('product_img');
            } else {
                return redirect('products/'.$id.'/edit')->with('fail','图片上传失败');
            }

        }
        $product->name = $request->get('name');
        if(isset($img_path)){
            $product->img_url = $img_path;
        }
        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->subcategory_id = $request->get('subcategory_id');
        $product->category_id = Subcategory::find($product->subcategory_id)->category->id;
        $product->save();

        return redirect('products')->with('success','已更新产品');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add_to_cart(){
        
    }

}
