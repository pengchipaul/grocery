<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Collection;
use App\Category;
use App\Subcategory;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\CollectionCollection;
use App\Http\Resources\Collection as CollectionResource;

class CollectionController extends Controller
{
    public function index(){
        $collections = Collection::all();
        return view('collections.index',compact('collections'));
    }

    public function create(){
        $collections = Collection::all();
        $categories = Category::all();
        return view('collections.create',compact('collections','categories','subcategories'));
    }

    public function store(Request $request){
        $collection = new Collection();
        if($request->hasfile('big_img_url'))
        {
            if($request->file('big_img_url')->isValid()){
                $big_img_path = $request->file('big_img_url')->store('collection_img');
            } else {
                return redirect(route('collections.index'))->with('fail','图片上传失败');
            }

        }
        if(isset($big_img_path)){
            $collection->big_img_url = $big_img_path;
        }
        if($request->hasfile('small_img_url'))
        {
            if($request->file('small_img_url')->isValid()){
                $small_img_path = $request->file('small_img_url')->store('collection_img');
            } else {
                return redirect(route('collections.index'))->with('fail','图片上传失败');
            }

        }
        if(isset($small_img_path)){
            $collection->small_img_url = $small_img_path;
        }
        $collection->name = $request->get('name');

        $collection->save();

        return redirect(route('collections.index'))->with('success','已添加类别');
    }

    public function edit(Request $request){
        $collection = Collection::find($request->id);
        return view('collections.edit',compact('collection'));
    }

    public function show($id){
        $collection = Collection::find($id);
        $products = $collection->products;
        return view('collections.show',compact('collection','products'));
    }

    public function update(Request $request ,$id){
        $collection = Collection::find($id);
        if($request->hasfile('big_img_url'))
        {
            if($request->file('big_img_url')->isValid()){
                $big_img_path = $request->file('big_img_url')->store('collection_img');
            } else {
                return redirect(route('collections.index'))->with('fail','图片上传失败');
            }

        }
        if(isset($big_img_path)){
            $collection->big_img_url = $big_img_path;
        }
        if($request->hasfile('small_img_url'))
        {
            if($request->file('small_img_url')->isValid()){
                $small_img_path = $request->file('small_img_url')->store('collection_img');
            } else {
                return redirect(route('collections.index'))->with('fail','图片上传失败');
            }

        }
        if(isset($small_img_path)){
            $collection->small_img_url = $small_img_path;
        }
        $collection->name = $request->get('name');

        $collection->save();

        return redirect(route('collections.index'))->with('success','更新类别成功');

    }

    /* For Api Call */
    public function api_index(){
        $collections = Collection::all();
        foreach ($collections as $collection){
            $collection['products'] = $collection->products->take(6);
            foreach($collection->categories as $category){
                $category['subcategories'] = $category->subcategories;
            }
            $collection['categories'] = $collection->categories;
        }
        return response()->json($collections,200);
    }

    public function api_show($id){
        $collection = Collection::find($id);
        $collection['products'] = $collection->products;
        foreach($collection->categories as $category){
            $category['subcategories'] = $category->subcategories;
        }
        $collection['categories'] = $collection->categories;
        return response()->json($collection,200);
    }

    public function api_show_by_sale($id){
        $collection = Collection::find($id);
        $collection['products'] = $collection->products->sortBy('sale')->take(6);
        return response()->json($collection,200);
    }

}
