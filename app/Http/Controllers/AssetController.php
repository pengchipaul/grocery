<?php

namespace App\Http\Controllers;

use App\Asset;
use DeepCopy\f002\A;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AssetController extends Controller
{
    public function index(){
        $assets = Asset::all();
        return view('assets.index',compact('assets'));
    }

    public function create(){
        return view('assets.create');
    }

    public function store(Request $request){
        $asset = new Asset();
        $asset->name = $request->get('name');
        if ($files = $request->file('data')) {
            $images = array();
            foreach ($files as $file) {
                Log::debug("testing uplaod image");
                if ($file->isValid()) {
                    $img_path = $file->store('img_assets');
                    $images[] = $img_path;
                } else {
                    return redirect(route('assets.index'))->with('fail', '图片上传失败');
                }
            }
            $asset->data = implode("|", $images);
            $asset->type = 'image';
            $asset->save();
            return redirect(route('assets.index'))->with('success','资源上传成功');
        } else {
            return redirect(route('assets.index'))->with('fail', '图片上传失败');
        }
    }

    public function edit($id){
        $asset = Asset::find($id);
        return view('assets.edit',compact('asset'));
    }

    public function update(Request $request, $id){
        $asset = Asset::find($id);
        $asset->name = $request->get('name');
        if ($files = $request->file('data')) {
            $images = array();
            foreach ($files as $file) {
                Log::debug("testing uplaod image");
                if ($file->isValid()) {
                    $img_path = $file->store('img_assets');
                    $images[] = $img_path;
                } else {
                    return redirect(route('assets.index'))->with('fail', '图片上传失败');
                }
            }
            $asset->data = implode("|", $images);
            $asset->type = 'image';
            $asset->save();
            return redirect(route('assets.index'))->with('success','资源上传成功');
        } elseif($files = $request->file('add_img')) {
            $images = explode('|',$asset->data);
            foreach ($files as $file) {
                Log::debug("testing uplaod image");
                if ($file->isValid()) {
                    $img_path = $file->store('img_assets');
                    $images[] = $img_path;
                } else {
                    return redirect(route('assets.index'))->with('fail', '图片上传失败');
                }
            }
            $asset->data = implode("|", $images);
            $asset->type = 'image';
            $asset->save();
            return redirect(route('assets.index'))->with('success','资源上传成功');
        } else {
            return redirect(route('assets.index'))->with('fail', '图片上传失败');
        }

    }

    public function api_show($id){
        $asset = Asset::find($id);
        $asset['img_urls'] = explode('|',$asset->data);
        return response()->json($asset,200);
    }
}
