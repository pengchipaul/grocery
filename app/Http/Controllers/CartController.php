<?php

namespace App\Http\Controllers;

use App\ShoppingCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function remove_item($id){
        $user = Auth::user();
        $sc = ShoppingCart::where(['user_id' => $user->id, 'product_id' => $id])->first();
        $sc->delete();
        return response()->json('remove successfully',200);
    }
}
