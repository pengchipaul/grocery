<?php

namespace App\Http\Controllers;

use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
    public function store(Request $request){
        $subcategory = new Subcategory();
        $subcategory->name = $request->name;
        $subcategory->category_id = $request->category_id;
        $subcategory->save();
        return redirect(route('collections.index'))->with('success','添加子分类成功');
    }

    public function show($id){
        $subcategory = Subcategory::find($id);
        $products = $subcategory->products;
        return view('subcategories.show',compact('subcategory','products'));
    }

    public function edit($id){
        $subcategory = Subcategory::find($id);
        $categories = Category::all();
        return view('subcategories.edit',compact('subcategory','categories'));
    }

    public function update(Request $request, $id){

    }

    public function api_show($id){
        $subcategory = Subcategory::find($id);
        return response()->json($subcategory->products,200);
    }
}
