<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller
{
    public function api_edit(Request $request, $id){
        $user = Auth::user();
        $address = $user->addresses->find($id);
        $address->update($request->all());
        return response()->json(['message'=>'success'],200);

    }
}
