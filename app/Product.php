<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ShoppingCart;

class Product extends Model
{
    protected $fillable =['name','description','price','category_id','sale','img_url'];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function subcategory(){
        return $this->belongsTo('App\Subcategory');
    }

    public function get_cart_quantity($user_id){
        return ShoppingCart::where('user_id', $user_id)->
            where('product_id',$this->id)->firstOrFail()->quantity;
    }

}
