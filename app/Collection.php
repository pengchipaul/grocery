<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    public function categories(){
        return $this->hasMany('App\Category');
    }

    public function products(){
        return $this->hasManyThrough('App\Product','App\Category');
    }
}
