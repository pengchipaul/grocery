<?php

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\ShoppingCart;
use App\Order;
use App\OrderProduct;
use App\Address;
use App\Favourite;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// User Model
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/user/register','UserController@api_register');

// Collection Model
Route::get('collections','CollectionController@api_index');
Route::get('collections/{id}','CollectionController@api_show');
Route::get('collections/by_sale/{id}','CollectionController@api_show_by_sale');

// Category Model
Route::get('categories',function(){
    $categories = Category::all();
   return response()->json($categories,200);
});

Route::get('categories/{id}','CategoryController@api_show');

// Subcategory Model
Route::get('subcategories/{id}','SubcategoryController@api_show');


// Product Model
Route::get('products',function(){
    $products = Product::all();
    return response()->json($products,200);
});
Route::get('products/{id}',function($id){
   $product = Product::find($id);
   return response()->json($product,200);
});


// Address Model
Route::middleware('auth:api')->get('user/addresses',function(Request $request){
   $user = $request->user();
   $addresses = $user->addresses;
   return response()->json($addresses,200);
});

Route::middleware('auth:api')->post('user/add_address',function(Request $request){
    Address::create(['name' => $request->name,
        'detail' => $request->detail,
        'user_id' => Auth::user()->id,
        'phone' => $request->phone,
        'detail_extension' => $request->detail_extension]);
    return response()->json(['message' => '添加地址成功'] ,200);
});
Route::post('user/addresses/edit/{id}','AddressController@api_edit')->middleware('auth:api');

// Order Model
Route::get('user/orders','OrderController@api_index')->middleware('auth:api');
Route::middleware('auth:api')->post('user/create_order',function(Request $request){
    $user = Auth::user();
    $shopping_cart = $user->shopping_cart;
    $total = 0;
    $quantity = 0;
    foreach ($shopping_cart as $product){
        $quantity += $product->quantity;
        $total += $product->quantity * Product::find($product->product_id)->price;
    }
    $order = new Order();
    $order->user_id = $user->id;
    $order->total = $total;
    $order->quantity = $quantity;
    $order->save();
    foreach ($shopping_cart as $product){
        OrderProduct::create(['order_id' => $order->id,
            'product_id' => $product->product_id,
            'quantity' => $product->quantity]);
    }
    ShoppingCart::where('user_id',$user->id)->delete();
    return response()->json(['message' => 'success','order_id' => $order->id],200);
});
Route::middleware('auth:api')->post('user/add_details_to_order',function(Request $request){
    $order = Order::where('user_id',Auth::user()->id)->find($request->order_id);
    $address = Address::where('user_id',Auth::user()->id)->find($request->address_id);
    $order->address_id = $address->id;
    $order->note = $request->note;
    $order->payment_type = $request->payment_type;
    $order->save();
    return response()->json(['message' => '添加订单收货地址信息成功'],200);
});
Route::get('user/orders/status/{status}','OrderController@api_get_orders_by_status')->middleware('auth:api');
Route::get('user/orders/{id}','OrderController@api_show')->middleware('auth:api');


// Favourite Model
Route::middleware('auth:api')->get('user/likes', function(Request $request){
    $user = $user = $request->user();
    $fav_products = $user->fav_products;
    return response()->json($fav_products,200);
});
Route::middleware('auth:api')->post('user/add_to_likes', function(Request $request){
    Favourite::create(['product_id' => $request->product_id,
        'user_id' => Auth::user()->id]);
    return response()->json(['message' => '收藏成功'],200);
});


// Shopping Cart Model
Route::middleware('auth:api')->get('user/shopping_cart', function(Request $request){
    $user = $request->user();
    $cart_products = $user->cart_products;
    foreach($cart_products as $product){
        $product['num'] = ShoppingCart::where('user_id',$user->id)->where('product_id',$product->id)->first()->quantity;
    }
    return response()->json($cart_products,200);
});
Route::middleware('auth:api')->post('user/add_to_cart', function(Request $request){
    $user = Auth::user();
    $cart_item = ShoppingCart::where('user_id',$user->id)->where('product_id',$request->product_id)->first();
    if($cart_item){
        $cart_item->quantity += $request->quantity;
        $cart_item->save();
    } else {
        ShoppingCart::create(['product_id' => $request->product_id,
            'quantity' => $request->quantity,
            'user_id' => $user->id]);
    }
    return response()->json(['message' => '加入购物车成功'] ,200);
});
Route::delete('user/shopping_cart/remove/{id}','CartController@remove_item')->middleware('auth:api');

// Image Asset
Route::get('assets/show/{id}','AssetController@api_show');
