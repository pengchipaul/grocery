<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// routes for user model
Route::resource('users','UserController')->middleware('admin');

// routes for product model
Route::resource('products','ProductController')->middleware('admin');

// routes for collection model
Route::get('collections','CollectionController@index')->middleware('admin')->name('collections.index');
Route::get('collections/create','CollectionController@create')->middleware('admin')->name('collections.create');
Route::post('collections/store','CollectionController@store')->middleware('admin')->name('collections.store');
Route::get('collections/edit/{id}','CollectionController@edit')->middleware('admin')->name('collections.edit');
Route::get('collections/show/{id}','CollectionController@show')->middleware('admin')->name('collections.show');
Route::patch('collections/update/{id}','CollectionController@update')->middleware('admin')->name('collections.update');

// routes for category model
Route::resource('categories','CategoryController')->middleware('admin');

// routes for subcategory model
Route::post('subcategories/store','SubcategoryController@store')->middleware('admin')->name('subcategories.store');
Route::get('subcategories/show/{id}','SubcategoryController@show')->middleware('admin')->name('subcategories.show');
Route::get('subcategories/edit/{id}','SubcategoryController@edit')->middleware('admin')->name('subcategories.edit');
Route::patch('subcategories/update/{id}','SubcategoryController@update')->middleware('admin')->name('subcategories.update');

// routes for assets
Route::get('assets','AssetController@index')->middleware('admin')->name('assets.index');
Route::get('assets/create','AssetController@create')->middleware('admin')->name('assets.create');
Route::post('assets/store','AssetController@store')->middleware('admin')->name('assets.store');
Route::get('assets/edit/{id}','AssetController@edit')->middleware('admin')->name('assets.edit');
Route::patch('assets/update/{id}','AssetController@update')->middleware('admin')->name('assets.update');
