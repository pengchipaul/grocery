<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopping_carts',function(Blueprint $table){
            $table->unique(['user_id','product_id']);
        });

        Schema::table('order_products',function(Blueprint $table){
            $table->unique(['order_id','product_id']);
        });

        Schema::table('favourites',function(Blueprint $table){
            $table->unique(['user_id','product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopping_carts',function(Blueprint $table){
            $table->index(['user_id','product_id']);
            $table->dropUnique(['user_id','product_id']);
        });

        Schema::table('order_products',function(Blueprint $table){
            $table->index(['order_id','product_id']);
            $table->dropUnique(['order_id','product_id']);
        });

        Schema::table('favourites',function(Blueprint $table){
            $table->index(['user_id','product_id']);
            $table->dropUnique(['user_id','product_id']);
        });
    }
}
