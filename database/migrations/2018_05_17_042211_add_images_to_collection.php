<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagesToCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collections',function (Blueprint $table){
           $table->string('big_img_url','100');
           $table->string('small_img_url','100');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collections',function(Blueprint $table){
           $table->dropColumn('big_img_url');
           $table->dropColumn('small_img_url');
        });
    }
}
