<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneToAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses',function(Blueprint $table){
            $table->string('phone','10');
            $table->string('detail_extension',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses',function(Blueprint $table){
            $table->dropColumn('phone');
            $table->dropColumn('detail_extension');

        });

    }
}
