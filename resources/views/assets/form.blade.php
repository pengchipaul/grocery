<div class="card">
    <div class="card-header">@if($action == 'create') 添加资源 @elseif($action == 'edit') 更新资源 @endif</div>
    <div class="card-body">
        <form method="post" @if($action == 'create') action="{{route('assets.store')}}"
              @elseif($action == 'edit') action="{{route('assets.update',['id'=>$asset->id])}}"
              @endif enctype="multipart/form-data">
            @csrf
            @if($action == 'edit')
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">图片</label>
                    <div class="col-md-6">
                        @foreach(explode('|',$asset->data) as $img )
                            <a href="{{asset('storage/app/'.'/'.$img)}}"><img src="{{asset('storage/app/').'/'.$img}}"
                                                                              style="width:100px;height:100px">
                            </a>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="form-group row">
                <label for="data" class="col-md-4 col-form-label text-md-right"> @if($action == 'create')
                        上传图片 @elseif($action == 'edit') 更新图片 @endif </label>

                <div class="col-md-6">
                    <input type="file"
                           class="form-control{{ $errors->has('images') ? ' is-invalid' : '' }}"
                           name="data[]" multiple @if($action == 'create') required @endif autofocus>

                    @if ($errors->has('images'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
            @if($action == 'edit')
                <div class="form-group row">
                    <label for="add_img" class="col-md-4 col-form-label text-md-right"> @if($action == 'create')
                            上传图片 @elseif($action == 'edit') 添加图片 @endif </label>

                    <div class="col-md-6">
                        <input type="file"
                               class="form-control{{ $errors->has('add_img') ? ' is-invalid' : '' }}"
                               name="add_img[]" multiple autofocus>

                        @if ($errors->has('add_img'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('add_img') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

            @endif
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">名称</label>

                <div class="col-md-6">
                    <input id="name" type="text"
                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                           name="name"
                           value="@if($action == 'create'){{ old('name') }}@elseif($action == 'edit'){{$asset->name}}@endif"
                           required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary btn-block">
                        @if($action == 'create') 添加 @elseif($action == 'edit') 更新 @endif
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>