@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            @include('assets.asset_tab')
        </div>
        @include('layouts.feedback')
        <table class="w-75 mx-auto table table-hover table-bordered text-center">
            <thead>
            <th>索引</th>
            <th>名称</th>
            <th>类型</th>
            <th>图片</th>
            <th>操作</th>
            </thead>
            @foreach($assets as $asset)
                <tr>
                    <td>{{$asset->id}}</td>
                    <td>{{$asset->name}}</td>
                    <td>图片</td>
                    <td>
                        @foreach(explode('|',$asset->data) as $img )
                            <a href="{{asset('storage/app/'.'/'.$img)}}" target="_blank"><img src="{{asset('storage/app/').'/'.$img}}" style="width:100px;height:100px">
                            </a>
                        @endforeach
                    </td>
                    <td>
                        <div class="btn-group-vertical">
                            <a href="{{route('assets.edit',['id'=>$asset->id])}}" class="btn btn-info">编辑</a>
                        </div>
                    </td>
                </tr>

            @endforeach
        </table>

    </div>

@endsection