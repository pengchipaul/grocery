@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            @include('assets.asset_tab')
        </div>

        <div class="container w-75">
            @include('assets.form',['action' => 'edit'])
        </div>
    </div>

@endsection