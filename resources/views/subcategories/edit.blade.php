@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('collections.collection_tab')
        </div>

        <hr>
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">更新子分类</div>
                <div class="card-body">
                    <form method="post" action="{{route('subcategories.update',['id' => $subcategory->id])}}"
                          enctype="multipart/form-data">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">名称</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       name="name" value="{{ $subcategory->name }}" required>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category_id" class="col-md-4 col-form-label text-md-right">分类</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}"
                                        name="category_id" required>
                                    <option>请选择分类</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" @if($category->id == $subcategory->category_id) selected @endif >{{$category->collection->name}}
                                            - {{$category->name}}  </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('category_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    添加
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
