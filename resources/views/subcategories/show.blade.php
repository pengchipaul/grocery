@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="container">
            <div class="row justify-content-center">
                @include('collections.collection_tab')
            </div>
        </div>

        <hr>
        @include('layouts.feedback')
        @include('products.product_table',['heading' => $subcategory->name])
    </div>

@endsection