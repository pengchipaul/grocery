@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('collections.collection_tab')
        </div>

        <hr>
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">更新类别</div>
                <div class="card-body">
                    <form method="post" action="{{action('CollectionController@update',$collection->id) }}"
                          enctype="multipart/form-data">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">
                        <div class="form-group row">
                            <label for="big_img_url" class="col-md-4 col-form-label text-md-right">更新首页大图片</label>

                            <div class="col-md-6">
                                <input id="big_img_url" type="file"
                                       class="form-control{{ $errors->has('big_img_url') ? ' is-invalid' : '' }}"
                                       name="big_img_url" autofocus>

                                @if ($errors->has('big_img_url'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('big_img_url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="small_img_url" class="col-md-4 col-form-label text-md-right">更新导航小图片</label>

                            <div class="col-md-6">
                                <input id="small_img_url" type="file"
                                       class="form-control{{ $errors->has('small_img_url') ? ' is-invalid' : '' }}"
                                       name="small_img_url" autofocus>

                                @if ($errors->has('small_img_url'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('small_img_url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">名称</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       name="name" value="{{ $collection->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    更新
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection