@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('collections.collection_tab')
        </div>
        <hr>
        @include('layouts.feedback')
        <table class="table table-bordered table-hover text-center">
            <thead>
            <tr>
                <th>
                    <h1>类别名称</h1>
                </th>
                <th>
                    <h1>首页大图片</h1>
                </th>
                <th>
                    <h1>导航栏小图片</h1>
                </th>
                <th>
                    <h1>操作</h1>
                </th>
            </tr>
            </thead>
            @foreach($collections as $collection)
                <tr>
                    <td><h2>{{$collection->name}}</h2></td>
                    <td><img src="storage/app/{{$collection['big_img_url']}}"
                             style="width:100px;height:100px"></td>
                    <td><img src="storage/app/{{$collection['small_img_url']}}"
                             style="width:100px;height:100px"></td>
                    <td>
                        <div class="btn-group-vertical d-flex">
                            <a href="{{route('collections.show',['id' => $collection->id])}}" class="btn btn-info">显示</a>
                            <a href="{{route('collections.edit',['id' => $collection->id])}}"
                               class="btn btn-info">编辑</a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <th>分类名称</th>
                    <th>/</th>
                    <th>操作</th>

                </tr>
                @foreach($collection->categories as $category)
                    <tr>
                        <td></td>
                        <td><h3>{{$category->name}}</h3> </td>
                        <td>{{$category->img_url}}</td>
                        <td>
                            <div class="btn-group-vertical d-flex">
                                <a href="{{route('categories.show',['id' => $category->id])}}" class="btn btn-info">显示</a>
                                <a href="{{route('categories.edit',['id' => $category->id])}}" class="btn btn-info">编辑</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>子分类名称</th>
                        <th>操作</th>
                    </tr>
                    @foreach($category->subcategories as $subcategory)
                        <tr>
                            <td></td>
                            <td></td>
                            <td>{{$subcategory->name}}</td>
                            <td>
                                <div class="btn-group-vertical d-flex">
                                    <a href="{{route('subcategories.show',['id' => $subcategory->id])}}" class="btn btn-info">显示</a>
                                    <a href="{{route('subcategories.edit',['id' => $subcategory->id])}}" class="btn btn-info">编辑</a>
                                </div>
                            </td>
                        </tr>

                    @endforeach
                @endforeach
            @endforeach

        </table>
    </div>

@endsection
