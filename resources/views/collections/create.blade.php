@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('collections.collection_tab')
            <hr class="w-100 m-2">
            <div class="container-fluid">
                <ul class="nav nav-tabs justify-content-center border-0" role="tablist">
                    <li class="nav-item">
                        <a class="btn btn-info" id="collection-tab" data-toggle="tab" href="#collection" role="tab"
                           aria-controls="collection" aria-selected="false">
                            类别
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-info" id="category-tab" data-toggle="tab" href="#category" role="tab"
                           aria-controls="category" aria-selected="false">
                            分类
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-info" id="subcategory-tab" data-toggle="tab" href="#subcategory" role="tab"
                           aria-controls="subcategory" aria-selected="false">
                            子分类
                        </a>
                    </li>
                </ul>
            </div>

            <div class="tab-content w-100">
                <div class="tab-pane fade" id="collection" role="tabpanel" aria-labelledby="collection-tab">
                    <div class="container-fluid">
                        <div class="card">
                            <div class="card-header">添加类别</div>
                            <div class="card-body">
                                <form method="post" action="{{ route('collections.store') }}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @include('layouts.feedback')
                                    <div class="form-group row">
                                        <label for="img_url" class="col-md-4 col-form-label text-md-right">首页大图片</label>

                                        <div class="col-md-6">
                                            <input id="big_img_url" type="file"
                                                   class="form-control{{ $errors->has('big_img_url') ? ' is-invalid' : '' }}"
                                                   name="big_img_url" required autofocus>

                                            @if ($errors->has('big_img_url'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('big_img_url') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="img_url" class="col-md-4 col-form-label text-md-right">导航小图片</label>

                                        <div class="col-md-6">
                                            <input id="small_img_url" type="file"
                                                   class="form-control{{ $errors->has('small_img_url') ? ' is-invalid' : '' }}"
                                                   name="small_img_url" required autofocus>

                                            @if ($errors->has('small_img_url'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('small_img_url') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">名称</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text"
                                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary btn-block">
                                                添加
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="category" role="tabpanel" aria-labelledby="category-tab">
                    <div class="container-fluid">
                        <div class="card">
                            <div class="card-header">添加分类</div>
                            <div class="card-body">
                                <form method="post" action="{{ route('categories.store') }}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @include('layouts.feedback')
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">名称</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text"
                                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="category" class="col-md-4 col-form-label text-md-right">类别</label>

                                        <div class="col-md-6">
                                            <select class="form-control{{ $errors->has('collection_id') ? ' is-invalid' : '' }}"
                                                    name="collection_id" value="{{ old('collection_id') }}" required
                                                    autofocus>
                                                @foreach($collections as $collection)
                                                    <option value="{{$collection->id}}">{{$collection->name}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('collection_id'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('collection_id') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary btn-block">
                                                添加
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="subcategory" role="tabpanel" aria-labelledby="subcategory-tab">
                    <div class="container-fluid">
                        <div class="card">
                            <div class="card-header">添加子分类</div>
                            <div class="card-body">
                                <form method="post" action="{{route('subcategories.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    @include('layouts.feedback')
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">名称</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text"
                                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="subcategory" class="col-md-4 col-form-label text-md-right">分类</label>

                                        <div class="col-md-6">
                                            <select class="form-control{{ $errors->has('collection_id') ? ' is-invalid' : '' }}"
                                                    name="category_id" value="{{ old('collection_id') }}" required
                                                    autofocus>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->collection->name}} - {{$category->name}}  </option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('collection_id'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('collection_id') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary btn-block">
                                                添加
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
