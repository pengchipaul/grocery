@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('categories.category_tab')
        </div>
        <hr>
        @include('layouts.feedback')
        <table class="table table-bordered table-hover text-center">
            <thead>
            <tr>
                <td>
                    <h1>名称</h1>
                </td>
                <td>
                    <h1>图片</h1>
                </td>
                <td>
                    <h1>操作</h1>
                </td>
            </tr>
            </thead>
            @foreach($categories as $category)
                <tr>
                    <td><h2>{{$category['name']}}</h2></td>
                    <td class="d-flex justify-content-center"><img src="storage/app/{{$category['img_url']}}"
                                                                   style="width:100px;height:100px"></td>
                    <td>
                        <div class="btn-group-vertical d-flex">
                            <a href="{{action('CategoryController@show', $category['id'])}}" class="btn btn-info">显示</a>
                            <a href="{{action('CategoryController@edit', $category['id'])}}" class="btn btn-info">编辑</a>
                        </div>
                    </td>
                </tr>

            @endforeach

        </table>
    </div>

@endsection
