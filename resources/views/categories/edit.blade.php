@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('collections.collection_tab')
            <hr>
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">编辑类别</div>
                    <div class="card-body">
                        <form method="post" action="{{ action('CategoryController@update',$id) }}"
                              enctype="multipart/form-data">
                            @csrf
                            @include('layouts.feedback')
                            <input name="_method" type="hidden" value="PATCH">

                            <div class="form-group row">
                                <label for="collection_id" class="col-md-4 col-form-label text-md-right">类别</label>
                                <div class="col-md-6">
                                    <select class="form-control {{ $errors->has('collection_id') ? ' is-invalid' : '' }}"
                                            name="collection_id" required>
                                        <option>请选择类别</option>
                                        @foreach($collections as $collection)
                                            <option value="{{$collection->id}}" @if($collection->id == $category->collection_id) selected @endif >{{$collection->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">名称</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ $category->name }}" required>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        更新
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection